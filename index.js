// alert("howdy");

function twoNumbers (number1, number2) {
	console.log("displayed sum of " + number1 + " and " + number2)
	let sum = number1 + number2;
	console.log(sum);
}
twoNumbers(5,15);

function twoNumbersMinus (numberMinus1, numberMinus2) {
	console.log("displayed difference of " +numberMinus1+ " and " + numberMinus2)
	let difference =numberMinus1 - numberMinus2;
	console.log(difference);
}

twoNumbersMinus(20, 5);

function product (num1, num2) {
	console.log("the product of " + num1 + " and " + num2);
	let product1=num1 * num2;
	return product1;
}
let prod = product(50,10);
console.log(prod);

function quotient (num1, num2) {
	console.log("the quotient of " + num1 + " and " + num2);
	let quotient1=num1 / num2;
	return quotient1;
}
let quoti = quotient(50,10);
console.log(quoti);


function circleArea(radius){
	const pi = 3.1416;
	let area= pi * (radius**2);
	console.log("the result of getting the area of a circle with 15 radius:")
	console.log(area);
}
circleArea(15);

function average(nume1, nume2, nume3, nume4) {
	console.log("the average of "+ nume1 +", "+ nume2 + ", " + nume3 + " and " + nume4 + ":");
	let ave=(nume1+nume2+nume3+nume4) / 4;
	return ave;
}
let ave1 = average(20, 40, 60, 80);
console.log(ave1);

function passing(numba1, numba2) {
	console.log("Is 38/50 a passing score?")
	let percentage = (numba1/numba2)*100;
	let isPassed= percentage >= 75;
	return isPassed;
}
let passing1 = passing(38,50);
console.log(passing1);